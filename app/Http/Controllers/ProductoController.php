<?php

namespace App\Http\Controllers;

use App\Models\Categoria;
use App\Models\Producto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductoController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function index()
    {
        return view('producto');
    }

    public function listarCategorias()
    {
        return Categoria::all();
    }

    public function listar()
    {
        return DB::table('productos')
            ->join('categorias', 'categorias.id', '=', 'productos.category_id')
            ->select('productos.*', DB::raw('categorias.name as cate_name'))
            ->orderBy('id', 'desc')->get();
        //return Producto::orderBy('id', 'desc')->get();
    }

    public function store(Request $request)
    {
        $data = request()->validate([
            'name' => ['required', 'string', 'max:255'],
            'price' => ['required', 'min:0'],
            'state' => ['required'],
            'category_id' => ['required', 'integer'],
        ]);
        Producto::create([
            'name' => $data['name'],
            'price' => $data['price'],
            'state' => $data['state'],
            'category_id' => $data['category_id']
        ]);
    }

    public function update(Request $request, Producto $idProducto)
    {
        $data = request()->validate([
            'name' => ['required', 'string', 'max:255'],
            'price' => ['required', 'min:0'],
            'state' => ['required'],
            'category_id' => ['required', 'integer'],
        ]);
        $idProducto->update($data);
    }

    public function delete(Producto $idProducto)
    {
        $idProducto->delete();
    }
}
