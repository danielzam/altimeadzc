@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <producto-component></producto-component>
            </div>
        </div>
    </div>
@endsection
