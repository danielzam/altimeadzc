<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductoController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Route::get('/productos', [ProductoController::class, 'index'])->name('productos');
Route::get('/listar_categorias', [ProductoController::class, 'listarCategorias'])->name('listar_categorias');
Route::get('/listar_productos', [ProductoController::class, 'listar'])->name('listar_productos');
Route::post('/crear_producto', [ProductoController::class, 'store'])->name('crear_producto');
Route::put('/editar_producto/{idProducto}', [ProductoController::class, 'update'])->name('editar_producto');
Route::delete('/eliminar_producto/{idProducto}', [ProductoController::class, 'delete'])->name('eliminar_producto');
